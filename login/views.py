from rest_framework.decorators import action
# Create your views here.
from django.views.decorators.csrf import csrf_exempt
from rest_framework.decorators import api_view, permission_classes
from django.contrib.auth import authenticate
from rest_framework import status, viewsets
from rest_framework.views import APIView
from rest_framework import generics
from rest_framework import mixins
from rest_framework.response import Response
from django.core.exceptions import ObjectDoesNotExist
from django.shortcuts import get_object_or_404
from .models import Account
from rest_framework.authtoken.models import Token
from .serializers import RegistrationSerializer, UserLoginSerializer, UserSerializer, ProfileSerializer
from rest_framework.permissions import AllowAny, IsAuthenticated
from rest_framework.authentication import TokenAuthentication


@csrf_exempt
@api_view(["POST"])
@permission_classes([AllowAny])
def registration_view(request):
    try:
        serializer = RegistrationSerializer(data=request.data)
        response = {}
        if serializer.is_valid():
            account = serializer.save()
            response['data'] = {'email': account.email}
            response['message'] = 'Successfully Registered !!'
            response['status'] = status.HTTP_201_CREATED
            response['error_status'] = 0
            response['exception_status'] = 0
        else:
            response['data'] = ''
            response['message'] = serializer.errors
            response['status'] = status.HTTP_200_OK
            response['error_status'] = 1
            response['exception_status'] = 0
        return Response(response)
    except Exception as e:
        response['data'] = ''
        response['message'] = str(e)
        response['status'] = status.HTTP_500_INTERNAL_SERVER_ERROR
        response['error_status'] = 0
        response['exception_status'] = 1
        return Response(response)


@api_view(["POST"])
@permission_classes([AllowAny])
def login_view(request):
    try:
        serializer = UserLoginSerializer(data=request.data)
        response = {}
        if serializer.is_valid():
            email = request.data['email']
            password = request.data['password']
            # user = authenticate(email=serializer.validated_data["email"],
            #                     password=serializer.validated_data["password"], token=None)
            user = authenticate(email=email, password=password)
            if user:
                token = Token.objects.get(user=user)

                response['data'] = {'email': user.email, 'token': token.key}
                response['message'] = 'Login Successful !!'
                response['status'] = status.HTTP_201_CREATED
                response['error_status'] = 0
                response['exception_status'] = 0
            else:
                response['data'] = ''
                response['message'] = 'Invalid Credentials!'
                response['status'] = status.HTTP_401_UNAUTHORIZED
                response['error_status'] = 1
                response['exception_status'] = 0
        else:
            response['data'] = ''
            response['message'] = 'Enter a valid email address'
            response['status'] = status.HTTP_401_UNAUTHORIZED
            response['error_status'] = 1
            response['exception_status'] = 0
        return Response(response)
    except Exception as e:
        response['data'] = ''
        response['message'] = str(e)
        response['status'] = status.HTTP_500_INTERNAL_SERVER_ERROR
        response['error_status'] = 0
        response['exception_status'] = 1
        return Response(response)


class ProfileViewset(viewsets.ViewSet):
    authentication_classes = [TokenAuthentication]

    # get request to fetch user details by ID
    @action(detail=True, methods=['get'], permission_classes=[IsAuthenticated])
    def retrieve_profile(self, request, pk=None):
        response = {}
        try:
            queryset = Account.objects.get(id=request.user.id)
            serializer = UserSerializer(queryset,context={"request": request})
            response["data"] = serializer.data
            response['message'] = 'Successful '
            response["status"] = status.HTTP_201_CREATED
            response['error_status'] = 0
            response['exception_status'] = 0
        except Exception as e:
            response['data'] = ''
            response['message'] = str(e)
            response['status'] = status.HTTP_500_INTERNAL_SERVER_ERROR
            response['error_status'] = 1
            response['exception_status'] = 1
        return Response(response)

    @action(detail=True, methods=['put'],permission_classes=[IsAuthenticated])
    def profile_update(self, request, pk=None):
        response = {}
        try:
            queryset = Account.objects.get(id=request.user.id)
            serializer = ProfileSerializer(queryset, data=request.data, partial=True)
            if serializer.is_valid():
                response["data"] = ''
                response['message'] = 'Profile Updated'
                response["status"] = status.HTTP_201_CREATED
                response['error_status'] = 0
                response['exception_status'] = 0
                serializer.save()
            else:
                response["data"] = ''
                response['message'] = 'Enter Valid inputs '
                response["status"] = status.HTTP_401_UNAUTHORIZED
                response['error_status'] = 1
                response['exception_status'] = 0
                return Response(response)
        except Exception as e:
            response['data'] = ''
            response['message'] = str(e)
            response['status'] = status.HTTP_500_INTERNAL_SERVER_ERROR
            response['error_status'] = 1
            response['exception_status'] = 1
        return Response(response)
