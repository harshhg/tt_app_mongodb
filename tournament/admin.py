from .models import Tournaments
from django.contrib import admin


class TournamentsAdmin(admin.ModelAdmin):
    search_fields = ['name']
    list_display = ('name','start_date','end_date','total_players')


# Register your models here.
admin.site.register(Tournaments, TournamentsAdmin)
